﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Assets.Scripts.Util;
using SimpleJSON;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class Leaderboard : MonoBehaviour
    {
        public GameObject ListElementPrefab;
        public HighscoresList HighscoresList;
        public Button PostScoreButton;
        public InputField PlayerNameText;
        public Text ScoreText;
        private int m_score;

        void Start ()
        {
            StartCoroutine(Refresh());
            PostScoreButton.onClick.AddListener(() => StartCoroutine(PostScore()));
        }

        public void Reset(int score)
        {
            ScoreText.text = string.Format("You collected {0} stars!", score);
            PostScoreButton.enabled = true;
            PlayerNameText.enabled = true;
            m_score = score;
        }

        private IEnumerator PostScore()
        {
            if(PlayerNameText.text.Length == 0) yield break;
            PostScoreButton.enabled = false;
            PlayerNameText.enabled = false;
            var score = new JSONClass();
            score["name"] = PlayerNameText.text;
            score["score"].AsInt = m_score;

            var encoding = new System.Text.UTF8Encoding();
            var headers = new Dictionary<string, string> { { "Content-Type", "application/json;charset=UTF-8" } };
            var request = new WWW(Constants.ScoresApiPath, encoding.GetBytes(score.ToString()), headers);
            yield return request;

            StartCoroutine(Refresh());
        }


        IEnumerator Refresh()
        {
            HighscoresList.Clear();
            var request = new WWW(Constants.ScoresApiPath);
            yield return request;
            var scoresJson = JSON.Parse(request.text).AsArray;
            var scores = new List<ListElement>();
            var placement = 1;
            foreach (var score in scoresJson.Childs)
            {
                var listElement = Instantiate(ListElementPrefab).GetComponent<ListElement>();
                listElement.NameText.text = score["name"];
                listElement.PlacementText.text = "" + placement++;
                listElement.ScoreText.text = score["score"];
                scores.Add(listElement);
            }
            HighscoresList.AddListElements(scores);
        }
    }
}
