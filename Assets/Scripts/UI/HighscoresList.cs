﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Util;
using SimpleJSON;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class HighscoresList : MonoBehaviour
    {

        public void AddListElements(ICollection<ListElement> elements)
        {
            var rectTransform = GetComponent<RectTransform>();
            foreach (var listElement in elements)
            {
                listElement.GetComponent<RectTransform>().SetParent(rectTransform, false);
            }
        }

        public void Clear()
        {
            var rectTransform = GetComponent<RectTransform>();
            foreach (var listElement in rectTransform.GetComponentsInChildren<ListElement>())
            {
                Destroy(listElement.gameObject);
            }
        }
    }
}
