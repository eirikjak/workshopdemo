﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class ListElement : MonoBehaviour
    {

        public Text PlacementText;
        public Text NameText;
        public Text ScoreText;
    }
}
