﻿using System;
using System.Collections.Generic;
using Assets;
using Assets.Scripts.Game;
using Assets.Scripts.UI;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{

    public Text ScoreText;
    public int m_score;
    public Leaderboard Leaderboard;
    private ICollection<PlayerPickup> m_pickups;

    void Start()
    {
        m_pickups = FindObjectsOfType<PlayerPickup>();
        AddPickupListeners();
        var playerDie = FindObjectOfType<PlayerDie>();
        playerDie.Die += OnPlayerDie;
        DisplayScore();
    }

    public void RestartGame()
    {
        Application.LoadLevel(0);
    }

    private void OnPlayerDie(object sender, EventArgs eventArgs)
    {
        Leaderboard.gameObject.SetActive(true);
        Leaderboard.Reset(m_score);
    }

    private void AddPickupListeners()
    {
        foreach (var pickup in m_pickups)
        {
            pickup.Pickup += Pickup;
        }
    }

    private void Pickup(object sender, EventArgs eventArgs)
    {

        m_score++;
        DisplayScore();
    }

    private void DisplayScore()
    {
        ScoreText.text = "Score:" + m_score;
        
    }


}
