﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts.Game
{
    public class PlayerDie : MonoBehaviour
    {

        public AudioClip DieClip;
        private AudioSource m_audioSource;
        private bool m_isDead;
        public event EventHandler Die;

        void Start()
        {
            m_audioSource = GetComponent<AudioSource>();
        }
        void Update () {
            if (transform.position.y < -10 && !m_isDead)
            {
                m_isDead = true;
                StartCoroutine(EndGame());
            }
            
        }

        private IEnumerator EndGame()
        {
            m_audioSource.clip = DieClip;
            m_audioSource.Play();
            yield return new WaitForSeconds(DieClip.length);
            OnDie();
        }

        protected virtual void OnDie()
        {
            EventHandler handler = Die;
            if (handler != null) handler(this, EventArgs.Empty);
        }
    }
}
