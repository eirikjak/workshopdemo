﻿using UnityEngine;

namespace Assets
{
    public class PlayerMove : MonoBehaviour
    {

        public float Speed;
        public float JumpForce;
        public AudioClip JumpSound;
        private Rigidbody2D m_rigidBody;
        private AudioSource m_audio;
        private bool m_canJump;
        void Start ()
        {
            m_rigidBody = GetComponent<Rigidbody2D>();
            m_audio = GetComponent<AudioSource>();
        }
	
        void Update () {
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                m_rigidBody.AddForce(new Vector2(-Speed, 0f), ForceMode2D.Impulse);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                m_rigidBody.AddForce(new Vector2(Speed, 0f), ForceMode2D.Impulse);
            }

            if (Input.GetKey(KeyCode.Space) && m_canJump)
            {
                m_canJump = false;
                m_rigidBody.AddForce(new Vector2(0f, JumpForce), ForceMode2D.Impulse);
                m_audio.clip = JumpSound;
                m_audio.Play();
            }
        }

        void OnCollisionEnter2D(Collision2D coll)
        {
            if (coll.gameObject.tag != "Ground") return;
            var groundPosition = coll.gameObject.transform.position;
            if (groundPosition.y < transform.position.y)
                m_canJump = true;
        }
    }
}
